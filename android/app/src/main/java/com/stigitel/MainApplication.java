package com.stigitel;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.shahenlibrary.RNVideoProcessingPackage;
import com.mybigday.rnmediameta.RNMediaMetaPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.dooboolab.RNAudioRecorderPlayerPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.goodatlas.audiorecord.RNAudioRecordPackage;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNVideoProcessingPackage(),
            new RNMediaMetaPackage(),
            new RNFetchBlobPackage(),
            new RNAudioRecorderPlayerPackage(),
            new ReactVideoPackage(),
            new RNSoundPackage(),
            new RNAudioRecordPackage(),
            new ReactNativeAudioPackage(),
            new ImagePickerPackage(),
            new VectorIconsPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
