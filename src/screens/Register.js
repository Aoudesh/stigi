import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,Button,
    AsyncStorage,TouchableOpacity,TextInput,StatusBar,Alert
} from "react-native";
//import Logo from '../component/Logo';
//import Background from '../component/Background';
class Register extends Component {
    static navigationOptions = { header: null }
     constructor(props){
    super(props)
    this.state={
      username:'',
      phonenumber:'',
      password:'',
      showPass:true
    }
  }
  async storeToken(accessToken) {
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, accessToken);
      console.log('Token stored');
    } catch (error) {
      console.log('Something went wrng');
    }
}
  userRegister = async() =>{
    const {username} = this.state;
    const{phonenumber}= this.state;
    const {password} = this.state;

    if(username=="" || password=="" || phonenumber==""){
      alert("Please fill out all fields");
    }
    // else if (password!=cpassword) {
    //     alert("Passwords do not match.");
    // }
    else{
      fetch('http://192.168.63.1/test/register.php', {
      method: 'post',
      header:{
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body:JSON.stringify({
        username: username,
        phonenumber:phonenumber,
        password: password,
       
      })
    })

    .then((response) => response.json())
      .then((responseJson) =>{
        if(responseJson == 'User Registered Successfully'){
          Alert.alert(
            'Success',
            'User Registered Successfully',
            [
              {text:'ok', onPress: () => this.props.navigation.navigate('Home')}
            ]
          );
          
          this.props.navigation.navigate('Home')
        }else{
          alert(responseJson);
        }
      })
      .catch((error)=>{
        console.log(error);
      });
  }
}
   render() {
    return (
    <View style={styles.container}>
    <TextInput
      placeholder="Username"
      style={{width:300,margin:10, borderColor:"#a7dfe2",borderWidth:1,borderRadius:20,
      padding:7,paddingHorizontal: 90,backgroundColor: 'rgba(255, 255, 255, 0.4)'}}
      placeholderTextColor='#232323'
      underlineColorAndroid="transparent"
      onChangeText= {username => this.setState({username})}
    />
     <TextInput
      placeholder="Phonenumber"
      style={{width:300,margin:10, borderColor:"#a7dfe2",borderWidth:1,borderRadius:20,
      padding:7,paddingHorizontal: 90,backgroundColor: 'rgba(255, 255, 255, 0.4)'}}
      placeholderTextColor='#232323'
      underlineColorAndroid="transparent"
      onChangeText= {phonenumber => this.setState({phonenumber})}
    />
    <TextInput
      placeholder="Password"
      style={{width:300,margin:10, borderColor:"#a7dfe2",borderWidth:1,borderRadius:20,
      padding:7,paddingHorizontal: 90,backgroundColor: 'rgba(255, 255, 255, 0.4)'}}
      placeholderTextColor='#232323'
      underlineColorAndroid="transparent"
      secureTextEntry={this.state.showPass}
      onChangeText= {password => this.setState({password})}
    />
      <TouchableOpacity
        onPress={this.userRegister.bind(this)}
        style={{alignItems: 'center',justifyContent: 'center',
        backgroundColor: '#d4e3fc',padding:5,paddingHorizontal:50,borderRadius:50}}>
          <Text style={{color:'black',fontWeight: 'bold',}}>Register</Text>
      </TouchableOpacity>
     </View>
   );
  }
}

export default  Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#000000',
    marginBottom: 5,
  },
});
