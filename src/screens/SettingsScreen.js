import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
    AsyncStorage,
    TouchableOpacity
} from "react-native";

class SettingsScreen extends Component {
     static navigationOptions = { header: null }

    signOut = async () => {
        AsyncStorage.clear()
        this.props.navigation.navigate('AuthLoading')
    }
   
    render() {
        return (
            <View style={styles.container}>
                <Button title="Sign Out" onPress={this.signOut} />
                <TouchableOpacity>
                <Text  onPress={() => this.props.navigation.navigate('About')}> About </Text>
                </TouchableOpacity>
                <Button title="Sign" />

            </View>
        );
    }
}
export default SettingsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});