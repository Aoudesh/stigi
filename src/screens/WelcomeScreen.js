import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,TouchableOpacity
} from "react-native";
import Logo from '../component/Logo';
import Background from '../component/Background';

class WelcomeScreen extends Component {
     static navigationOptions = { header: null }
    render() {
        return (
             <Background>
             <Logo/>
            <View style={styles.container}>
                <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText} onPress={() => this.props.navigation.navigate('SignIn')}> SignIn </Text>
               </TouchableOpacity> 
               <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText} onPress={() => this.props.navigation.navigate('SignUp')}> SignUp </Text>
               </TouchableOpacity> 
            </View>
            </Background>
        );
    }
}
export default WelcomeScreen
    ;

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
     button: {
    width:75,
    backgroundColor:'#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
});