import React, { Component } from "react";
import {View,Text,Button,Image,StyleSheet,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'


export default class DrawerScreen extends Component{

	render(){
		return(
			<View style={styles.container}>
			<Image style={styles.Image}
            source={require('../images/defalut.png')} />
            <Text> @username </Text>
            <View>
             <Icon
               name='ios-settings'
               color='#000'
               size={14}
               />
           <TouchableOpacity>
                <Text  onPress={() => this.props.navigation.navigate('Settings')}> Setting </Text>
               </TouchableOpacity> 
          </View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    Image:{
    	width:70,
    	height:80, 
    	borderRadius:40
    }	
});
