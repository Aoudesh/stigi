import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,Dimensions,Image
} from "react-native";
import {Header,Left,Right,Icon,Container} from 'native-base'
import {Avatar} from 'react-native-elements'
class HomeScreen extends Component {
    static navigationOptions = { header: null }
    render() {
        return (
          <Container style={{flex:1,backgroundColor:'#3q455c'}}>
          <Header style={{ width: Dimensions.get('window').width,}}>
          <Left>
           <Avatar
            small
            rounded
            source={require('../images/defalut.png')}
            // onPress={() => console.log("Works!")}
            activeOpacity={0.7}
            />
          </Left>
          <View style={{alignItems: 'center',justifyContent:'center',flexDirection:'row'}}>
            <Text style={{justifyContent: 'center', color: 'white', alignContent:'center',marginRight:-30}}>
            AudiStigian
            </Text>
           </View>
          <Right>
          <Icon name='person-add' style={{color:'white'}}/>
          </Right>
          </Header>
            <Container>
            <View style={styles.container}>
            <Left style={styles.left}>
            <Avatar
            small
            rounded
            source={require('../images/defalut.png')}
            // onPress={() => console.log("Works!")}
            activeOpacity={0.7}
            />
            <View>
            <Text>username</Text>
            <Text style={styles.title}>this is my tittle</Text>
            <Text>This is my description my first description</Text>
            </View>
           </Left>
          </View>
           <View style={styles.container1}>
           <Image style={styles.images}
            source={require('../images/frog.jpg')} />
            </View>
            </Container>
        </Container>

        );
    
}}
export default HomeScreen;
const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    container1:{
        alignContent:'center',
        justifyContent:'center',
        marginRight:-35
    },
    images:{
        width:200,
        height:150,
        marginLeft:-15,
    },
    left:{
        flexDirection:'column',
        flexDirection:'row',
        alignContent:'space-between'
    },
    title:{
        alignContent:'space-between'
    }
});
