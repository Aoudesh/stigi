import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,Button,
    AsyncStorage,TouchableOpacity,TextInput,StatusBar,Alert
} from "react-native";
import Logo from '../component/Logo';
import Background from '../component/Background';
class SignUpScreen extends Component {
    static navigationOptions = { header: null }
     constructor(props){
    super(props)
    this.state={
      username:'',
      phonenumber:'',
      password:'',
      showPass:true
    }
  }
  async storeToken(accessToken) {
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, accessToken);
      console.log('Token stored');
    } catch (error) {
      console.log('Something went wrng');
    }
}
  userRegister = async() =>{
    const {username} = this.state;
    const{phonenumber}= this.state;
    const {password} = this.state;

    if(username=="" || password=="" || phonenumber==""){
      alert("Please fill out all fields");
    }
    // else if (password!=cpassword) {
    //     alert("Passwords do not match.");
    // }
    else{
      fetch('http://192.168.63.1/test/register.php', {
      method: 'post',
      header:{
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body:JSON.stringify({
        username: username,
        phonenumber:phonenumber,
        password: password,
       
      })
    })

    .then((response) => response.json())
      .then((responseJson) =>{
        if(responseJson == 'User Registered Successfully'){
          Alert.alert(
            'Success',
            'User Registered Successfully',
            [
              {text:'ok', onPress: () => this.props.navigation.navigate('Home')}
            ]
          );
          
          this.props.navigation.navigate('Home')
        }else{
          alert(responseJson);
        }
      })
      .catch((error)=>{
        console.log(error);
      });
  }
}
    render() {
        return (
             <Background>
        <StatusBar
        backgroundColor="#002f6c"
        barStyle="light-content"
        />
            <Logo/>
              <View style={styles.container}>
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Username"
              placeholderTextColor = "#ffffff"
              selectionColor="#fff"
              keyboardType="email-address"
             onChangeText= {username => this.setState({username})}
              />
              <TextInput style={styles.inputBox} maxLength={10}
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="PhoneNumber"
              placeholderTextColor = "#ffffff"
              selectionColor="#fff"
              keyboardType="number-pad"
              onChangeText= {phonenumber => this.setState({phonenumber})}
              />
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#ffffff"
              ref={(input) => this.password = input}
              onChangeText= {password => this.setState({password})}
              />  
            <TouchableOpacity style={styles.button} onPress={this.userRegister} >
              <Text style={styles.buttonText} >SignUp </Text>
           </TouchableOpacity>     
             </View>
            </Background>
        );
    }
}
export default SignUpScreen;
const styles = StyleSheet.create({
    container: {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
    },
    inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#ffffff',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
});