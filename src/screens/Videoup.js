import React, { Component } from "react";
import {
    View, 
    Text,
    StyleSheet,Dimensions,TouchableOpacity,Image,Alert
} from "react-native";
import { Container, Content, Icon, Header, Left, Body, Right, Segment, Button } from 'native-base'
import ImagePicker from 'react-native-image-picker';
import MediaMeta from 'react-native-media-meta';
const options = {
  title: 'Choose Video',
  mediaType:'video',
  takePhotoButtonTitle:'Record',
  chooseFromlibraryButtonTitle:'choose from gallery',
  durationLimit: 180, // 3 mins - Works only when you are recording
  allowsEditing: true,
  videoQuality: 'high',
  storageOptions: { // if this key is provided, the image will get saved in the documents/pictures directory (rather than a temporary directory)
    skipBackup: true, // image will NOT be backed up to icloud
    path: 'images' // will save image at /Documents/images rather than the root
  }
};

class Videoup extends Component <{}>{
    static navigationOptions = { header: null }

    constructor(){
        super()
        this.state ={
            VideoSource:null
        }
    }
    selectPhoto(){
               ImagePicker.showImagePicker(options, (video) => {
      const path = video.path; // for android
      // const path = video.uri.substring(7); // for ios
      const maxTime = 180000; // 3 min
      MediaMeta.get(path)
        .then((metadata) => {
          if (metadata.duration > maxTime ) {
               Alert.alert(
              'Sorry',
              'Video duration must be less then 3 minutes',
              [
                { text: 'OK', onPress: () => console.log('OK Pressed') }
              ],
              { cancelable: false }
            );
          } else {
            // Upload or do something else
            
          }
        }).catch(err => console.error(err));
    });
    }
    render() {
        return (
         <Container style={styles.container}>
                <Header style={{width: Dimensions.get('window').width, paddingLeft: 10, paddingLeft: 10 ,color:'black'}}>
                    <Left> 
                         <TouchableOpacity>
                        <Icon name="ios-arrow-round-back" />
                        </TouchableOpacity>
                    </Left>
                    <Body><Text style={{ fontSize: 20,color:'white' }}>StigiUploadVideo</Text></Body>
                </Header>
                <View> 
                     <Image style={{width:300, height:250}}
                     source={this.state.VideoSource !=null ? this.state.VideoSource:
                     require('../images/video.png')} />
                </View>
                <View style={{flex:1,flexDirection: 'row', justifyContent: 'space-between',alignItems:'flex-end'}}>
                  <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                <Text> Image </Text>
               </TouchableOpacity> 
               <TouchableOpacity onPress={() => this.props.navigation.navigate('ReImage')}>
                <Text> ReImage </Text>
               </TouchableOpacity>
                </View>
         </Container>
        );
    }
}
export default Videoup;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        color:'white'
    }
});
 