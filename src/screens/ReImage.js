import React, { Component } from "react";
import {
    View, 
    Text,
    StyleSheet,Dimensions,TouchableOpacity,Image
} from "react-native";
import { Container, Content, Icon, Header, Left, Body, Right, Segment, Button } from 'native-base'
import ImagePicker from 'react-native-image-picker';
import Audio from './Audio'

const options = {
  title: 'Select Image',
  takePhotoButtonTitle:'take a photo',
  chooseFromlibraryButtonTitle:'choose from gallery',
  quality:1 
};

class ReImage extends Component <{}>{
    static navigationOptions = { header: null }

    constructor(){
        super()
        this.state ={
            ImageSource:null
        }
    }
    selectPhoto(){
                ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);

            if (response.didCancel) {
            console.log('User cancelled image picker');
            } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            } else {
            let source = { uri: response.uri };
  
            this.setState({
              ImageSource: source,
            });
            }
            });
    }
    render() {
        return (
         <Container style={styles.container}>
                <Header style={{width: Dimensions.get('window').width, paddingLeft: 10, paddingLeft: 10 ,color:'black'}}>
                    <Left> 
                         <TouchableOpacity>
                        <Icon name="ios-arrow-round-back" />
                        </TouchableOpacity>
                    </Left>
                    <Body><Text style={{ fontSize: 20,color:'white' }}>StigiUploadReImage</Text></Body>
                </Header>
                <View style={{ flex:2,alignItems:'center',justifyContent:'center',marginTop:5}}> 
                    <Image style={{width:300, height:250}}
                     source={require('../images/default.jpeg')} />
                  <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                <Text> Choose/Capture</Text>
               </TouchableOpacity>
                <TouchableOpacity>
                <Text> Audio </Text>
                 </TouchableOpacity>
                </View>
                <Audio/>
                <View style={{flex:1,flexDirection: 'row', justifyContent: 'space-between',alignItems:'flex-end'}}>
                  <TouchableOpacity >
                <Text> Image </Text>
               </TouchableOpacity> 
               <TouchableOpacity  onPress={() => this.props.navigation.navigate('Videoup')} >
                <Text> Video </Text>
               </TouchableOpacity>
                </View>
         </Container>
        );
    }
}
export default ReImage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        color:'white'
    }
});
 