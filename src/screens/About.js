import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,StatusBar
} from "react-native";

class About extends Component <{}>{
    static navigationOptions = { header: null }
    render() {
        return (

            <View style={styles.container}>
               <StatusBar
        backgroundColor="#002f6c"
        barStyle="light-content"
        />
                <Text>About</Text>
            </View>
        );
    }
}
export default About;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
