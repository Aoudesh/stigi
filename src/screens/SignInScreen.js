import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
    AsyncStorage,TouchableOpacity,TextInput,StatusBar
} from "react-native";
import Logo from '../component/Logo';
import Background from '../component/Background';


class SignInScreen extends Component {
    
    signIn = async () => {
        await AsyncStorage.setItem('userToken', 'user_id')

        this.props.navigation.navigate('App')
    }

    render() {
        return (
        <Background>
        <StatusBar
        backgroundColor="#002f6c"
        barStyle="light-content"
        />
            <Logo/>
              <View style={styles.container}>
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Email/PhoneNumber"
              placeholderTextColor = "#ffffff"
              selectionColor="#fff"
              keyboardType="email-address"
              onSubmitEditing={()=> this.password.focus()}
              />
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#ffffff"
              ref={(input) => this.password = input}
              />  
           <TouchableOpacity style={styles.button} >
             <Text style={styles.buttonText} onPress={this.signIn}>SignIn </Text>
           </TouchableOpacity>     
             </View>
            </Background>
        );
    }
}
export default SignInScreen;

const styles = StyleSheet.create({
    container: {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
    },
    inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#ffffff',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
});