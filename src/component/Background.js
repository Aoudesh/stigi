import React,{ Component } from 'react';
import { AppRegistry , StyleSheet, View, Text, ImageBackground } from 'react-native';

export default class Background extends Component {
  render() {
    return (

      <ImageBackground
        source={require('../images/background.png')}
        style={styles.backgroundImage}>
              {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    }
});

AppRegistry.registerComponent('Stigitel', () => Background);