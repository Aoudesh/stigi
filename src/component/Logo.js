import React, {Component} from 'react';
import{Image, StyleSheet, Text, View, StatusBar } from 'react-native';

export default class Logo extends Component{
	render(){
		return(
			<View style={styles.container}>
           <Image style={{width:70, height:70}}
            source={require('../images/stgi.jpg')} />
			<Text style={styles.logoText}>Welcome to StigiTel</Text>
			</View>
			)
	}
}
const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'flex-end',
    alignItems: 'center'
  },
  logoText : {
  	marginVertical: 15,
  	fontSize:18,
  	color:'rgba(255, 255, 255, 0.7)'
  }
});